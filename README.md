### Documentation template

If you use this template, you can create a static HTML webpage of whatever documentation you save in here.  It creates a nice indexed structure like this https://www.mkdocs.org/user-guide/writing-your-docs/.

### Using this templates in Our Sci

To use this template in your group, you need to:

1. Change these two lines in mkdocs.yml file in the main folder
   - site_name: Our-Sci GitLab Pages -- Template <-- add your site name here
   - site_url: https://our-sci.gitlab.io/subgroupName/mkdocs/ <-- change this to your subgroupName
   - For example, this template static site is: https://our-sci.gitlab.io/templates/mkdocs/

Now you can add markdown files to your repository and the index will match the folder structure of your files!

### Learn more about mkdocs generally 

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.
